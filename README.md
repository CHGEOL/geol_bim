# GEOL_BIM
Bei [GEOL_BIM](https://chgeol.org/geol_bim/) handelt es sich um ein von der Schweizerischen Agentur für Innovationsförderung Innosuisse mitfinanziertes Innovationsprojekt (Projektnummer 40458.1 IP-ENG). Für die Umsetzung des Innosuisse-Projekts [GEOL_BIM](https://chgeol.org/geol_bim/) sind die Landesgeologie von swisstopo und die Fachhochschule Nordwestschweiz FHNW unter der Leitung des Schweizer Geologenverband CHGEOL verantwortlich. Unterstützt wird [GEOL_BIM](https://chgeol.org/geol_bim/) zudem durch namhafte Ämter, Organisationen und Unternehmen.

## Das Projekt GEOL_BIM vereint Wissen für sicheres Bauen

**Building Information Modeling BIM ist die Zukunft des digitalen Bauens. BIM verändert die Planungs- und Arbeitsprozesse und erfordert für eine erfolgreiche Umsetzung eine Vielzahl qualifizierter Informationen aus den unterschiedlichsten Bereichen. In BIM begegnen uns die Herausforderungen von Big Data, die auch vor der Geologie-Szene nicht haltmachen.**

![](Abbildungen/SwisstopoUntergrund.jpg)

Mit dem Innovationsprojekt GEOL_BIM werden künftig raumbezogene geologischen Daten (3D-Modelle, GIS-Welt) mit detaillierten Gebäudeinformationen (BIM-Welt) im digitalen Bauen ergänzt. Innosuisse, die Schweizer Agentur zur Förderung von wissensbasierten Innovationsprojekten im Interesse von Wirtschaft und Gesellschaft, hat im Dezember 2019 den Antrag für die Finanzierungsunterstützung für das Projekt GEOL_BIM bewilligt. Das Projekt ist Anfang März 2020 gestartet und soll bis im Frühjahr 2022 abgeschlossen sein. Für die Umsetzung zeichnen sich die Landesgeologie von swisstopo und die Fachhochschule Nordwestschweiz FHNW unter der Leitung des Schweizer Geologenverband CHGEOL verantwortlich.

Namhafte Ämter, Organisationen und Unternehmen sind am Projekt GEOL_BIM beteiligt. Mit dem Amt für Wald und Naturgefahren Graubünden, dem Bundesamt für Strassen ASTRA, dem Büro für Technische Geologie BTG, der De Cérenville Géotechnique SA, der Dr. von Moos AG, der GeoMod ingénieurs conseils SA, der Geologik AG, der GEOTEST AG, Geotechnik Schweiz, Jäckli Geologie AG, der Konferenz Geologischer Untergrund KGU, der Lombardi Group GmbH, der Nationale Genossenschaft für die Lagerung radioaktiver Abfälle NAGRA, der Schweizerische Bundesbahnen SBB und der Vereinigung Kantonaler Gebäudeversicherungen VKG arbeiten qualifizierte Fachleute beim Projekt GEOL_BIM mit und tragen mit ihren Use Cases dazu bei, dass die entwickelten Lösungen auch praxistauglich sein werden.

Die einzelnen Projektschritte sind:
- Standardisierung der Erfassung von geologischen Daten
- Bereitstellung eines konzeptuellen Modells für den Datentransfer von geologischen Informationen in BIM
- Entwicklung von Schnittstellen und Software-Tools für den Austausch geologischer Informationen mit BIM
- Strukturierung von geologischen Informationen aus Praxisprojekten und die Schnittstellen und Software-Tools zusammen mit Bauwerksmodel| BIM anzuwenden, zu verbessern und zu validieren
- Ausarbeitung eines Weiterbildungs- und Schulungskonzepts

Ziel des Projektes ist der Import eines geologischen 3D-Modells in eine BIM-Software und die Erstellung geologischer Profilschnitte mit Angabe der Prognosegenauigkeit an einem beliebigen Punkt. Die Ergebnisse werden anhand von Use Cases aus der Praxis getestet und decken folgende Anwendungen ab:

- Die oben genannte Grundfunktion erlaubt beispielsweise die Ausarbeitung von verschiedenen Linienführungen, die Mengenermittlung für die Bewirtschaftung von auszuhebendem Material sowie die Planung von zusätzlichen Untersuchungen zur Verbesserung der Prognosequalität.
- Ebenso sollen Baugrundkennwerte in die BIM-Software transferiert werden. Damit wird die direkte Übernahme der Baugrundkennwerte für die Modellierung und Dimensionierung von Fundationen und Baugrubensicherungen ermöglicht.
- Der Transfer von Informationen zu Gefährdungen und Intensitäten aus gravitativen Prozessen in die BIM Software unterstützt schliesslich die Planung von Schutzbauwerken im Nahbereich des Objekts. Für die Betriebsphase des Bauwerks sollen Messresultate aus der Überwachung von permanenten Rutschungen im BIM verfügbar gemacht werden, so dass präventive Unterhaltsarbeiten geplant werden können. Weiter soll die Rückführung von Massenverschiebungen aus der Betriebsphase zurück in das geologische Modell ermöglicht werden, so dass stets ein aktualisiertes geologisches 3D Modell verfügbar bleibt.

Nach Abschluss des Projekts im Jahr 2022 soll GEOL_BIM möglichst breit eingesetzt werden. Der CHGEOL wird Praxisschulungen im Umgang mit GEOL_BIM anbieten. Auch sollen schrittweise Erweiterungen durchgeführt werden, damit der prognostizierte volkswirtschaftliche Nutzen bis ins Jahr 2030 erreicht werden kann. Dieser wird auf rund 250 Mio. Schweizer Franken geschätzt.

Für weitere Fragen steht der Projektleiter Dr. Johannes Graf, Schweizer Geologenverband CHGEOL (johannes.graf@chgeol.ch) zur Verfügung.

## Ergebnisse

### Transformatoren

- Bohrungen nach IFC ([Prototyp](https://gitlab.com/CHGEOL/geol_bim-bhl2ifc))
- Grenzflächen nach IFC ([1. Prototyp](https://gitlab.com/CHGEOL/geol_bim-geo2ifc), [2. Prototyp](https://gitlab.com/CHGEOL/geol_bim-gf2ifc))
- Voxel nach IFC ([Prototyp](https://gitlab.com/CHGEOL/geol_bim-vxl2ifc))

### Webanwendung

[Erster Prototyp](https://gitlab.com/CHGEOL/ifcwebservice)

Zweiter Prototyp
- [UI](https://gitlab.com/CHGEOL/geol_bim-server-ui)
- [API](https://gitlab.com/CHGEOL/geol_bim-server-api) 
- [DB](https://gitlab.com/CHGEOL/geol_bim-server-db) 

## Projektpartner

Mitfinanziert durch:

<img src="Logos/LogoInnosuisse.png" width="300"/>

Projektleitung & Haupt-Umsetzungspartner:

<img src="Logos/LogoChgeol.png" width="300"/>

Umsetzungspartner:

<img src="Logos/LogoSwisstopo.png" width="300"/>

Forschungspartner:

<img src="Logos/LogoFhnw.png" width="300"/>